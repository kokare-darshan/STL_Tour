CC = g++
EXE = -o
OBJ = -c
HEADER = -I include/
CFALGS = -Wall
SRC = src
BUILD_DIR = build/

map:
	$(CC) $(EXE) $(BUILD_DIR)$@ $(HEADER)  $(SRC)/map.cpp
clean:
	rm -rf *.o build/*
